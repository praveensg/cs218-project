/* -*-  Mode: C++; c-file-style: "gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2009 University of Washington
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

//
// This program configures a grid (default 5x5) of nodes on an 
// 802.11b physical layer, with
// 802.11b NICs in adhoc mode, and by default, sends one packet of 1000 
// (application) bytes to node 1.
//
// The default layout is like this, on a 2-D grid.
//
// n20  n21  n22  n23  n24
// n15  n16  n17  n18  n19
// n10  n11  n12  n13  n14
// n5   n6   n7   n8   n9
// n0   n1   n2   n3   n4
//
// the layout is affected by the parameters given to GridPositionAllocator;
// by default, GridWidth is 5 and numNodes is 25..
//
// There are a number of command-line options available to control
// the default behavior.  The list of available command-line options
// can be listed with the following command:
// ./waf --run "iridium --help"
//
// Note that all ns-3 attributes (not just the ones exposed in the below
// script) can be changed at command line; see the ns-3 documentation.
//
// For instance, for this configuration, the physical layer will
// stop successfully receiving packets when distance increases beyond
// the default of 500m.
// To see this effect, try running:
//
// ./waf --run "iridium --distance=500"
// ./waf --run "iridium --distance=1000"
// ./waf --run "iridium --distance=1500"
// 
// The source node and sink node can be changed like this:
// 
// ./waf --run "iridium --sourceNode=20 --sinkNode=10"
//
// This script can also be helpful to put the Wifi layer into verbose
// logging mode; this command will turn on all wifi logging:
// 
// ./waf --run "iridium --verbose=1"
//
// By default, trace file writing is off-- to enable it, try:
// ./waf --run "iridium --tracing=1"
//
// When you are done tracing, you will notice many pcap trace files 
// in your directory.  If you have tcpdump installed, you can try this:
//
// tcpdump -r iridium-0-0.pcap -nn -tt
//

#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/mobility-module.h"
#include "ns3/config-store-module.h"
#include "ns3/wifi-module.h"
#include "ns3/internet-module.h"
#include "ns3/olsr-helper.h"
#include "ns3/aodv-helper.h"
#include "ns3/ipv4-static-routing-helper.h"
#include "ns3/ipv4-list-routing-helper.h"
#include "ns3/netanim-module.h"

#include "ns3/flow-monitor-helper.h"
#include "ns3/flow-classifier.h"
#include "ns3/ipv4-flow-classifier.h"
#include "ns3/nstime.h"
#include "ns3/aodv-helper.h"
#include "ns3/applications-module.h"

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <cstdlib>
#include <sstream>

using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("WifiIrridumGrid");

void ReceivePacket (Ptr<Socket> socket);
static void jumpSatellite (Ptr<Node> satNode);
std::vector<std::vector<double> > readCordinatesFile (std::string node_coordinates_file_name);
static void GenerateTraffic (Ptr<Socket> socket, uint32_t pktSize, uint32_t pktCount, Time pktInterval );

int main (int argc, char *argv[])
{
  std::string phyMode ("DsssRate1Mbps");
  double distance = 30;  // m
  uint32_t packetSize = 1000; // bytes
  uint32_t numPackets = 100;
  uint32_t numSatellites = 68;  // by default, 11x6
  //uint32_t numTerminals = 2; 
  uint32_t sinkNode = 67; 
  uint32_t sourceNode = 66;
  double interval = 1.0; // seconds
  bool verbose = false;
  bool tracing = false;

  CommandLine cmd;

  cmd.AddValue ("phyMode", "Wifi Phy mode", phyMode);
  cmd.AddValue ("distance", "distance (m)", distance);
  cmd.AddValue ("packetSize", "size of application packet sent", packetSize);
  cmd.AddValue ("numPackets", "number of packets generated", numPackets);
  cmd.AddValue ("interval", "interval (seconds) between packets", interval);
  cmd.AddValue ("verbose", "turn on all WifiNetDevice log components", verbose);
  cmd.AddValue ("tracing", "turn on ascii and pcap tracing", tracing);
  cmd.AddValue ("numSatellites", "number of satellite nodes", numSatellites);
  cmd.AddValue ("sinkNode", "Receiver node number", sinkNode);
  cmd.AddValue ("sourceNode", "Sender node number", sourceNode);

  cmd.Parse (argc, argv);

  //---------------- Setting Defaults ---------------//
  Time interPacketInterval = Seconds (interval);

  // disable fragmentation for frames below 2200 bytes
  //Config::SetDefault ("ns3::WifiRemoteStationManager::FragmentationThreshold", StringValue ("2200"));
  // turn off RTS/CTS for frames below 2200 bytes
  //Config::SetDefault ("ns3::WifiRemoteStationManager::RtsCtsThreshold", StringValue ("2200"));
  // Fix non-unicast data rate to be the same as that of unicast
  Config::SetDefault ("ns3::WifiRemoteStationManager::NonUnicastMode", 
                      StringValue (phyMode));



  //---------------- Creating Satellite and terminal Nodes ----------------//
  NodeContainer satellites;
  satellites.Create (numSatellites);

  //NodeContainer terminals;
  //terminals.Create (numTerminals);

  NodeContainer allNodes;
  //allNodes.Add(terminals);
  allNodes.Add(satellites);
  // AnimationInterface *pAnim = 0;
  // pAnim->UpdateNodeColor(66, 0, 255, 0);
  // pAnim->UpdateNodeColor(67, 0, 0, 255);

  //----------------- Creating wifi NICs --------------------//
  // The below set of helpers will help us to put together the wifi NICs we want
  WifiHelper wifi;
  if (verbose)
    {
      wifi.EnableLogComponents ();  // Turn on all Wifi logging
    }

  YansWifiPhyHelper wifiPhy =  YansWifiPhyHelper::Default ();
  wifiPhy.Set ("RxGain", DoubleValue (-35) ); 
  wifiPhy.SetPcapDataLinkType (YansWifiPhyHelper::DLT_IEEE802_11_RADIO); 

  NqosWifiMacHelper wifiMac = NqosWifiMacHelper::Default ();
  wifiMac.SetType ("ns3::AdhocWifiMac");
  wifi.SetStandard (WIFI_PHY_STANDARD_80211b);
  wifi.SetRemoteStationManager ("ns3::ConstantRateWifiManager",
                                "DataMode",StringValue (phyMode),
                                "ControlMode",StringValue (phyMode));

  YansWifiChannelHelper wifiChannel;
  wifiChannel.SetPropagationDelay ("ns3::ConstantSpeedPropagationDelayModel");
  wifiChannel.AddPropagationLoss ("ns3::FriisPropagationLossModel");

  wifiPhy.SetChannel (wifiChannel.Create ());
  //NetDeviceContainer satelliteDevices = wifi.Install (wifiPhy, wifiMac, satellites);
  //wifiPhy.Set("RxGain", DoubleValue (-40) );
  //wifiPhy.SetChannel (wifiChannel.Create ());
  NetDeviceContainer allNodeDevices = wifi.Install (wifiPhy, wifiMac, allNodes);
  


  //--------------- Mobility of Satellite and Terminal Nodes -----------------//
  // Satellite Mobility
  std::vector<std::vector<double> > coord_array;
  std::string node_coordinates_file_name ("scratch/node_coordinates.txt");
  coord_array = readCordinatesFile (node_coordinates_file_name);

  MobilityHelper satelliteMobility;
  Ptr<ListPositionAllocator> positionAlloc = CreateObject<ListPositionAllocator> ();
  for (size_t m = 0; m < 68; m++)
    {
      positionAlloc->Add (Vector (coord_array[m][0], coord_array[m][1], 0));
    }
  satelliteMobility.SetPositionAllocator (positionAlloc);

  satelliteMobility.SetMobilityModel ("ns3::ConstantVelocityMobilityModel");
  satelliteMobility.Install (satellites);
  for (uint n=0 ; n < 68 ; n++)
     {
       	Ptr<ConstantVelocityMobilityModel> cvMob = satellites.Get(n)->GetObject<ConstantVelocityMobilityModel>();
        if (n<33)
       	   cvMob->SetVelocity(Vector(0, 0.06, 0));
        else if (n<66) 
          cvMob->SetVelocity(Vector(0, -0.06, 0));
        else 
          cvMob->SetVelocity(Vector(0, 0.00, 0));
     }

  // Terminal Node Mobility
  // MobilityHelper terminalMobility;
  // terminalMobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  // terminalMobility.Install (terminals);

  // int posx = -90, posy = 30;
  // for (uint n=0 ; n < terminals.GetN() ; n++)
  //    {
  //       Ptr<MobilityModel> mob = terminals.Get(n)->GetObject<MobilityModel>();
  //       mob->SetPosition(Vector(posx + n*distance*3, posy - n*distance*2, 0));
  //    }



  //------------------ Installing IPv4 Internet Stack -------------------//
  // Enable OLSR
  OlsrHelper olsr;
  // AodvHelper olsr;
  Ipv4StaticRoutingHelper staticRouting;

  Ipv4ListRoutingHelper list;
  list.Add (staticRouting, 0);
  list.Add (olsr, 10);

  InternetStackHelper internet;
  internet.SetRoutingHelper (list);
  //internet.Install (satellites);
  //internet.Install (terminals);
  internet.Install (allNodes);

  Ipv4AddressHelper ipv4;
  NS_LOG_INFO ("Assign IP Addresses.");
  //ipv4.SetBase ("10.1.1.0", "255.255.255.0");
  //Ipv4InterfaceContainer satelliteInterfaces = ipv4.Assign (satelliteDevices);
  ipv4.SetBase ("10.1.2.0", "255.255.255.0");
  Ipv4InterfaceContainer allNodeInterfaces = ipv4.Assign (allNodeDevices);

  /*for (uint32_t i = 0; i < allNodes.GetN(); ++i)  
  {
    Ptr<Node> node = allNodes.Get (i); // Get pointer to ith node in container
    Ptr<Ipv4> ipv4 = node->GetObject<Ipv4> (); // Get Ipv4 instance of the node
    Ipv4Address addr = ipv4->GetAddress (1, 0).GetLocal (); // Get Ipv4InterfaceAddress of xth interface.
    std::cout << addr <<std::endl;
  }*/
  
  TypeId tid = TypeId::LookupByName ("ns3::UdpSocketFactory");
  Ptr<Socket> recvSink = Socket::CreateSocket (allNodes.Get (sinkNode), tid);
  InetSocketAddress local = InetSocketAddress (Ipv4Address::GetAny (), 80);
  recvSink->Bind (local);
  recvSink->SetRecvCallback (MakeCallback (&ReceivePacket));

  Ptr<Socket> source = Socket::CreateSocket (allNodes.Get (sourceNode), tid);
  InetSocketAddress remote = InetSocketAddress (allNodeInterfaces.GetAddress (sinkNode, 0), 80);
  source->Connect (remote);



  //-------------- Tracing the routes and neightbours -------------------//
  if (tracing == true)
    {
      AsciiTraceHelper ascii;
      wifiPhy.EnableAsciiAll (ascii.CreateFileStream ("iridium.tr"));
      wifiPhy.EnablePcap ("iridium", allNodeDevices);
      Ptr<OutputStreamWrapper> routingStream = Create<OutputStreamWrapper> ("iridium.routes", std::ios::out);
      olsr.PrintRoutingTableAllEvery (Seconds (2), routingStream);
      Ptr<OutputStreamWrapper> neighborStream = Create<OutputStreamWrapper> ("iridium.neighbors", std::ios::out);
      olsr.PrintNeighborCacheAllEvery (Seconds (2), neighborStream);
    }



  //---------------- Generating Traffic ---------------------//
  // Give OLSR time to converge-- 30 seconds perhaps
  Simulator::Schedule (Seconds (10.0), &GenerateTraffic, 
                       source, packetSize, numPackets, interPacketInterval);


  
  //---------------- Scheduling Satellite Jumps ------------------//
  for (uint n=0 ; n < satellites.GetN() ; n++)
    {
      Ptr<MobilityModel> mob = satellites.Get(n)->GetObject<MobilityModel>();
      Vector position = mob->GetPosition();
      Vector velocity = mob->GetVelocity();
      if (velocity.y !=0){
      Time eventTime;
      if (velocity.y > 0)
        eventTime = Seconds((90-position.y)/velocity.y);
      else eventTime = Seconds((90+position.y)/(-1*velocity.y));
      Simulator::Schedule (eventTime, &jumpSatellite, satellites.Get(n));
    }
    }



  //---------------- Simulation ------------------//
  NS_LOG_UNCOND ("Testing from node " << sourceNode << " to " << sinkNode << " with grid distance " << distance);

  FlowMonitorHelper flowmon;
  Ptr<FlowMonitor> monitor = flowmon.InstallAll();

  AnimationInterface anim ("iridium_latest.xml");
  Simulator::Stop (Seconds (31.0 + interval*numPackets));
  Simulator::Run ();

  monitor->SerializeToXmlFile("iridiumlatest_stats.xml", true, true);
  monitor->CheckForLostPackets();

  // Print the statistics from the flow monitor including transmitted and received time and throughput
  Ptr<Ipv4FlowClassifier> classifier = DynamicCast <Ipv4FlowClassifier>(flowmon.GetClassifier());
  std::map<FlowId, FlowMonitor::FlowStats> stats = monitor->GetFlowStats();
  
  for (std::map<FlowId, FlowMonitor::FlowStats>::const_iterator i = stats.begin(); i != stats.end(); ++i)
  {
    if(i->first >= 1)
    {
      Ipv4FlowClassifier::FiveTuple t = classifier->FindFlow (i->first);
      // Time txtime = i->second.timeFirstTxPacket;
      std::cout << "Time of first transmitted packet " << i->second.timeFirstTxPacket << std::endl;
      std::cout << "Time of last received packet " << i->second.timeLastRxPacket << std::endl; 
      std::cout << "Flow " << i->first - 2 << " (" << t.sourceAddress << "->" << t.destinationAddress << ")" << std::endl;
      std::cout << " Transmitted bytes:    " << i->second.txBytes << std::endl;
      std::cout << " Received bytes:    " << i->second.rxBytes << std::endl;
      // std::cout << "Throughput:   " << i->second.rxBytes * 8.0 / 10.0 / 1024 / 1024 << " Mbps" << std::endl;
      NS_LOG_UNCOND("Throughput: " << i->second.rxBytes * 8.0 / (i->second.timeLastRxPacket.GetSeconds()-i->second.timeFirstTxPacket.GetSeconds()) / 1024  << " Kbps");
    }
  }

  Simulator::Destroy ();

  return 0;
}



static void jumpSatellite (Ptr<Node> satNode) 
{
  Ptr<ConstantVelocityMobilityModel> cvMob = satNode->GetObject<ConstantVelocityMobilityModel>();
  Ptr<MobilityModel> mob = satNode->GetObject<MobilityModel>();
  Vector m_position = mob->GetPosition();
  Vector m_velocity = mob->GetVelocity();
  
  Time eventTime;
  if (m_velocity.y > 0)
  {
    m_position.x = 180 + m_position.x;
    m_velocity.y *= -1;
    eventTime = Seconds((90+m_position.y)/(-1*m_velocity.y));
  }
  else
  { 
    m_position.x = m_position.x - 180;
    m_velocity.y *= -1;
    eventTime = Seconds((90-m_position.y)/(1*m_velocity.y));
  }
        
  mob->SetPosition(m_position);
  cvMob->SetVelocity(m_velocity);

  Simulator::Schedule (eventTime, &jumpSatellite, satNode);
}


void ReceivePacket (Ptr<Socket> socket)
{
  while (socket->Recv ())
    {
      // NS_LOG_UNCOND ("Received one packet!");
    }
}


static void GenerateTraffic (Ptr<Socket> socket, uint32_t pktSize, 
                             uint32_t pktCount, Time pktInterval )
{
  if (pktCount > 0)
    {
      socket->Send (Create<Packet> (pktSize));
      Simulator::Schedule (pktInterval, &GenerateTraffic, 
                           socket, pktSize,pktCount-1, pktInterval);
    }
  else
    {
      socket->Close ();
    }
}


std::vector<std::vector<double> > readCordinatesFile (std::string node_coordinates_file_name)
{
  std::ifstream node_coordinates_file;
  node_coordinates_file.open (node_coordinates_file_name.c_str (), std::ios::in);
  if (node_coordinates_file.fail ())
    {
      NS_FATAL_ERROR ("File " << node_coordinates_file_name.c_str () << " not found");
    }
  std::vector<std::vector<double> > coord_array;
  int m = 0;

  while (!node_coordinates_file.eof ())
    {
      std::string line;
      getline (node_coordinates_file, line);

      if (line == "")
        {
          NS_LOG_WARN ("WARNING: Ignoring blank row: " << m);
          break;
        }

      std::istringstream iss (line);
      double coordinate;
      std::vector<double> row;
      int n = 0;
      while (iss >> coordinate)
        {
          row.push_back (coordinate);
          n++;
        }

      if (n != 2)
        {
          NS_LOG_ERROR ("ERROR: Number of elements at line#" << m << " is "  << n << " which is not equal to 2 for node coordinates file");
          exit (1);
        }

      else
        {
          coord_array.push_back (row);
        }
      m++;
    }
  node_coordinates_file.close ();
  return coord_array;

}

